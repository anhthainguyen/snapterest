var React = require('react');
var ReactDOM = require('react-dom');
var content = <ul className="list-item">
  <li className="item-1">This is item 1</li>
  <li className="item-2">This is item 2</li>
  <li className="item-3">This is item 3</li>
  </ul>;
ReactDOM.render(content, document.getElementById('react-application'));
